import socket
from core.messages import colored, bcolors


class Client(object):
    def __init__(self, host, port):
        self.host = host
        self.port = int(port)
        self.clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.clientSocket.connect(('127.0.0.1', 1027))
        except:
            raise ValueError('Server is down!')

    def send_data(self):
        while True:
            if not colored:
                data = input("Command: ")
            else:
                data = input(bcolors.WARNING + "Command: " + bcolors.ENDC)

            if data == 'exit':
                break

            byte_data = bytes(data, encoding='utf-8')
            self.clientSocket.send(byte_data)
            byte_received_data = self.clientSocket.recv(4096)
            received_data = byte_received_data.decode('utf-8')
            print(received_data)

        self.clientSocket.close()
        if colored:
            print(bcolors.UNDERLINE + bcolors.BOLD + bcolors.HEADER + "\n======== GOODBYE ========\n" + bcolors.ENDC)
        else:
            print("\n======== GOODBYE ========\n")

if __name__ == '__main__':
    client = Client('127.0.0.1', int(1027))
    client.send_data()
