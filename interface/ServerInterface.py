import socket
import threading
from core.ServerCore import ServerCore


class ThreadedServer(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            client.settimeout(120)
            threading.Thread(target=self.listen_to_client, args=(client, address)).start()

    def listen_to_client(self, client, address):
        clientcore = ServerCore()
        size = 1024
        while True:
            # try:
            bytes_data = client.recv(size)
            data = bytes_data.decode('utf-8')
            if data == 'exit':
                client.close()
            if data:
                result = clientcore.interpret(command=data)
                byte_result = bytearray(result, encoding='utf-8')
                client.send(byte_result)
            # except:
            #     client.close()
            #     raise ValueError('Listening failed')


def init_server():
    ThreadedServer('', int(1027)).listen()
