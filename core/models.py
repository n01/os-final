import peewee

db = peewee.SqliteDatabase('sandbox.db')


class BaseModel(peewee.Model):
    class Meta:
        database = db


class User(BaseModel):
    username = peewee.CharField(unique=True)
    password = peewee.CharField()


class ShareInfo(BaseModel):
    master_file = peewee.CharField(max_length=500)
    slave_file = peewee.CharField(max_length=500)
    master_user = peewee.CharField()
    slave_user = peewee.CharField()
