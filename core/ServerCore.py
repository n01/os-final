import os
import shutil
import fcntl

from .messages import *
from .models import *


# TODO: write-char
class ServerCore:
    def __init__(self):
        self.user = None
        self.cwd = '/'

    def interpret(self, command):
        args = command.split(':')

        if args[0] == 'new-account' and len(args) == 3:
            if self.user is None:
                result = self.create_account(args[1], args[2])
                return result
            else:
                return EXTRA_MESSAGE_1
        elif args[0] == 'rem-account' and len(args) == 3:
            if self.user is None:
                result = self.remove_account(args[1], args[2])
                return result
            else:
                return EXTRA_MESSAGE_1
        elif args[0] == 'login' and len(args) == 3:
            if self.user is None:
                result = self.login(args[1], args[2])
                return result
            else:
                return EXTRA_MESSAGE_1
        elif args[0] == 'logout' and len(args) == 1:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.logout()
                return result
        elif args[0] == 'where-am-i' and len(args) == 1:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.where_am_i()
                return result
        elif args[0] == 'goto' and len(args) == 2:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.goto(args[1])
                return result
        elif args[0] == 'new-file' and len(args) == 3:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.new_file(args[1], args[2])
                return result
        elif args[0] == 'rem-file' and len(args) == 3:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.rem_file(args[1], args[2])
                return result
        elif args[0] == 'ren-file' and len(args) == 4:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.ren_file(args[1], args[2], args[3])
                return result
        elif args[0] == 'read-file' and len(args) == 3:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.read_file(args[1], args[2])
                return result
        elif args[0] == 'write-file' and len(args) == 4:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.write_file(args[1], args[2], args[3])
                return result
        elif args[0] == 'mv-file' and len(args) == 5:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.mv_file(args[1], args[2], args[3], args[4])
                return result
        elif args[0] == 'cp-file' and len(args) == 5:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.cp_file(args[1], args[2], args[3], args[4])
                return result
        elif args[0] == 'new-dir' and len(args) == 3:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.new_dir(args[1], args[2])
                return result
        elif args[0] == 'rem-dir' and len(args) == 3:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.rem_dir(args[1], args[2])
                return result
        elif args[0] == 'ren-dir' and len(args) == 4:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.ren_dir(args[1], args[2], args[3])
                return result
        elif args[0] == 'mv-dir' and len(args) == 5:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.mv_dir(args[1], args[2], args[3], args[4])
                return result
        elif args[0] == 'cp-dir' and len(args) == 5:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.cp_dir(args[1], args[2], args[3], args[4])
                return result
        elif args[0] == 'ls-dir' and len(args) == 3:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.ls_dir(args[1], args[2])
                return result
        elif args[0] == 'share-file' and len(args) == 4:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.share_file(args[1], args[2], args[3])
                return result
        elif args[0] == 'unshare-file' and len(args) == 4:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.unshare_file(args[1], args[2], args[3])
                return result
        elif args[0] == 'share-dir' and len(args) == 4:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.share_dir(args[1], args[2], args[3])
                return result
        elif args[0] == 'unshare-dir' and len(args) == 4:
            if self.user is None:
                return MESSAGE_1
            else:
                result = self.unshare_dir(args[1], args[2], args[3])
                return result
        else:
            return 'Invalid Command!'

    @staticmethod
    def create_account(username, password):
        try:
            user = User.get(User.username == username)
            return MESSAGE_3
        except User.DoesNotExist:
            pass

        mdir = 'sandbox/' + username
        newuser = User(username=username, password=password)
        newuser.save()
        os.makedirs(mdir)
        return MESSAGE_2

    @staticmethod
    def remove_account(username, password):
        try:
            mdir = 'sandbox/' + username
            user = User.get((User.username == username) & (User.password == password))
            user.delete_instance()
            ServerCore.remove_account_cleanup(username)
            shutil.rmtree(mdir)
            return MESSAGE_4
        except User.DoesNotExist:
            return MESSAGE_5
        except FileNotFoundError:
            return MESSAGE_4

    def login(self, username, password):
        try:
            user = User.get((User.username == username) & (User.password == password))
            self.user = user
            return MESSAGE_6
        except User.DoesNotExist:
            return MESSAGE_5

    def logout(self):
        self.user = None
        return MESSAGE_7

    def where_am_i(self):
        if colored:
            return bcolors.OKGREEN + 'Ok: ' + bcolors.ENDC + self.cwd + bcolors.OKGREEN + '.' + bcolors.ENDC
        else:
            return 'Ok: ' + self.cwd + '.'

    def goto(self, path):
        mrootdir = os.path.abspath('sandbox/' + self.user.username)
        if path == '':
            mnewdir = os.path.normpath(mrootdir)
        elif path[0] == '/':
            mnewdir = os.path.normpath(mrootdir + path)
        else:
            mnewdir = os.path.normpath(mrootdir + self.cwd + '/' + path)

        if not os.path.isdir(mnewdir):
            return MESSAGE_9

        mreldir = os.path.relpath(mnewdir, mrootdir)
        if ('../' in mreldir) or (mreldir == '.'):
            self.cwd = '/'
        else:
            self.cwd = '/' + mreldir

        if colored:
            return bcolors.OKGREEN + 'Ok: ' + bcolors.ENDC + self.cwd + bcolors.OKGREEN + '.' + bcolors.ENDC
        else:
            return 'Ok: ' + self.cwd + '.'

    def get_abs_path(self, path):
        mrootdir = os.path.abspath('sandbox/' + self.user.username)
        if path == '':
            mnewdir = os.path.normpath(mrootdir + self.cwd)
        elif path[0] == '/':
            mnewdir = os.path.normpath(mrootdir + path)
        else:
            mnewdir = os.path.normpath(mrootdir + self.cwd + '/' + path)

        mreldir = os.path.relpath(mnewdir, mrootdir)
        if '../' in mreldir:
            raise ValueError('Path is wrong')

        return mnewdir

    # File Management ----------------------------------------------------

    def new_file(self, path, filename):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mfilepath = mabspath + '/' + filename

        if os.path.isfile(mfilepath):
            return MESSAGE_11
        elif os.path.isdir(mfilepath):
            return EXTRA_MESSAGE_2

        os.mknod(mfilepath)
        return MESSAGE_10

    def rem_file(self, path, filename):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mfilepath = mabspath + '/' + filename

        if not os.path.isfile(mfilepath):
            return MESSAGE_13

        if os.path.islink(mfilepath):
            self.slave_remove(mfilepath)
        else:
            self.master_remove(mfilepath)

        os.remove(mfilepath)
        return MESSAGE_12

    def ren_file(self, path, oldname, newname):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        moldpath = mabspath + '/' + oldname
        mnewpath = mabspath + '/' + newname

        if os.path.islink(moldpath):
            return MESSAGE_34

        if not os.path.isfile(moldpath):
            return MESSAGE_13

        if os.path.isfile(mnewpath):
            return MESSAGE_11
        elif os.path.isdir(mnewpath):
            return EXTRA_MESSAGE_2

        os.rename(moldpath, mnewpath)
        return MESSAGE_14

    def mv_file(self, oldpath, oldname, newpath, newname):
        try:
            moabspath = self.get_abs_path(oldpath)

            if not os.path.isdir(moabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        try:
            mnabspath = self.get_abs_path(newpath)

            if not os.path.isdir(mnabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        moldpath = moabspath + '/' + oldname
        mnewpath = mnabspath + '/' + newname

        if not os.path.isfile(moldpath):
            return MESSAGE_13

        if os.path.isfile(mnewpath):
            return MESSAGE_11
        elif os.path.isdir(mnewpath):
            return EXTRA_MESSAGE_2

        if os.path.islink(moldpath):
            self.slave_change(moldpath, mnewpath)
        else:
            self.master_change(moldpath, mnewpath)

        os.rename(moldpath, mnewpath)
        return MESSAGE_21

    def cp_file(self, oldpath, oldname, newpath, newname):
        try:
            moabspath = self.get_abs_path(oldpath)

            if not os.path.isdir(moabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        try:
            mnabspath = self.get_abs_path(newpath)

            if not os.path.isdir(mnabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        moldpath = moabspath + '/' + oldname
        mnewpath = mnabspath + '/' + newname

        if not os.path.isfile(moldpath):
            return MESSAGE_13

        if os.path.isfile(mnewpath):
            return MESSAGE_11
        elif os.path.isdir(mnewpath):
            return EXTRA_MESSAGE_2

        if os.path.islink(moldpath):
            self.slave_copy(moldpath, mnewpath)

        shutil.copyfile(moldpath, mnewpath)
        return MESSAGE_22

    def read_file(self, path, filename):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mfilepath = mabspath + '/' + filename

        if not os.path.isfile(mfilepath):
            return MESSAGE_13

        with open(mfilepath, 'r') as content_file:
            fcntl.flock(content_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
            content = content_file.read()
            fcntl.flock(content_file, fcntl.LOCK_UN)

        if colored:
            return bcolors.OKGREEN + 'Ok: ' + bcolors.ENDC + content + bcolors.OKGREEN + '.' + bcolors.ENDC
        else:
            return 'Ok: ' + content + '.'

    def write_file(self, path, filename, data):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mfilepath = mabspath + '/' + filename

        if not os.path.isfile(mfilepath):
            return MESSAGE_13

        with open(mfilepath, 'w') as content_file:
            fcntl.flock(content_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
            content_file.write(data)
            fcntl.flock(content_file, fcntl.LOCK_UN)

        return MESSAGE_20

    # Directory Management -----------------------------------------------

    def new_dir(self, path, filename):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mfilepath = mabspath + '/' + filename

        if os.path.isdir(mfilepath):
            return MESSAGE_24
        elif os.path.isfile(mfilepath):
            return EXTRA_MESSAGE_3

        os.mkdir(mfilepath)
        return MESSAGE_23

    def rem_dir(self, path, filename):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mfilepath = mabspath + '/' + filename

        if not os.path.isdir(mfilepath):
            return MESSAGE_26

        if os.path.islink(mfilepath):
            self.slave_remove(mfilepath)
            mcwdpath = os.path.abspath('sandbox/' + self.user.username) + self.cwd
            if mcwdpath.startswith(mfilepath):
                self.cwd = ''
            os.remove(mfilepath)
            return MESSAGE_25
        else:
            self.master_remove(mfilepath)

        mcwdpath = os.path.abspath('sandbox/' + self.user.username) + self.cwd
        if mcwdpath.startswith(mfilepath):
            self.cwd = '/'

        shutil.rmtree(mfilepath)
        return MESSAGE_25

    def ren_dir(self, path, oldname, newname):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        moldpath = mabspath + '/' + oldname
        mnewpath = mabspath + '/' + newname

        if os.path.islink(moldpath):
            return MESSAGE_34

        if not os.path.isdir(moldpath):
            return MESSAGE_26

        if os.path.isdir(mnewpath):
            return MESSAGE_24
        elif os.path.isfile(mnewpath):
            return EXTRA_MESSAGE_3

        mcwdpath = os.path.abspath('sandbox/' + self.user.username) + self.cwd
        if mcwdpath.startswith(moldpath):
            self.cwd = '/'

        os.rename(moldpath, mnewpath)
        return MESSAGE_27

    def mv_dir(self, oldpath, oldname, newpath, newname):
        try:
            moabspath = self.get_abs_path(oldpath)

            if not os.path.isdir(moabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        try:
            mnabspath = self.get_abs_path(newpath)

            if not os.path.isdir(mnabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        moldpath = moabspath + '/' + oldname
        mnewpath = mnabspath + '/' + newname

        if not os.path.isdir(moldpath):
            return MESSAGE_26

        if os.path.isdir(mnewpath):
            return MESSAGE_24
        elif os.path.isfile(mnewpath):
            return EXTRA_MESSAGE_3

        if os.path.islink(moldpath):
            self.slave_change(moldpath, mnewpath)
        else:
            self.master_change(moldpath, mnewpath)

        mcwdpath = os.path.abspath('sandbox/' + self.user.username) + self.cwd
        if mcwdpath.startswith(moldpath):
            self.cwd = '/'

        os.rename(moldpath, mnewpath)
        return MESSAGE_32

    def cp_dir(self, oldpath, oldname, newpath, newname):
        try:
            moabspath = self.get_abs_path(oldpath)

            if not os.path.isdir(moabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        try:
            mnabspath = self.get_abs_path(newpath)

            if not os.path.isdir(mnabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        moldpath = moabspath + '/' + oldname
        mnewpath = mnabspath + '/' + newname

        if not os.path.isdir(moldpath):
            return MESSAGE_26

        if os.path.isdir(mnewpath):
            return MESSAGE_24
        elif os.path.isfile(mnewpath):
            return EXTRA_MESSAGE_3

        if os.path.islink(moldpath):
            self.slave_copy(moldpath, mnewpath)

        shutil.copytree(moldpath, mnewpath)
        return MESSAGE_22

    def ls_dir(self, path, dirname):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mpath = mabspath + '/' + dirname

        if not os.path.isdir(mpath):
            return MESSAGE_26

        mlist = sorted(os.listdir(mpath))
        mstr = ""
        isfirst = True
        for name in mlist:
            if os.path.isdir(mpath + '/' + name):
                if isfirst:
                    isfirst = False
                else:
                    mstr += ', '
                mstr = mstr + name

        for name in mlist:
            if os.path.isfile(mpath + '/' + name):
                if isfirst:
                    isfirst = False
                else:
                    mstr += ', '
                mstr = mstr + name

        if colored:
            return bcolors.OKGREEN + 'Ok: ' + bcolors.ENDC + mstr + bcolors.OKGREEN + '.' + bcolors.ENDC
        else:
            return 'Ok: ' + mstr + '.'

    # Share Management ---------------------------------------------------

    def share_file(self, path, filename, username):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mmasterfilepath = mabspath + '/' + filename

        if not os.path.isfile(mmasterfilepath):
            return MESSAGE_13
        elif os.path.islink(mmasterfilepath):
            return EXTRA_MESSAGE_4

        try:
            user = User.get((User.username == username))
        except User.DoesNotExist:
            return MESSAGE_16

        mslavefilepath = os.path.abspath('sandbox/' + username + '/' + filename)
        if os.path.exists(mslavefilepath):
            return MESSAGE_11

        try:
            shareinfo = ShareInfo.get((ShareInfo.master_file == mmasterfilepath) & (ShareInfo.slave_user == username))
            if shareinfo is not None:
                return EXTRA_MESSAGE_5
        except ShareInfo.DoesNotExist:
            pass

        if username == self.user.username:
            return EXTRA_MESSAGE_6

        newinfo = ShareInfo(master_user=self.user.username, slave_user=username, master_file=mmasterfilepath,
                            slave_file=mslavefilepath)
        newinfo.save()
        os.symlink(mmasterfilepath, mslavefilepath, False)
        return MESSAGE_15

    def unshare_file(self, path, filename, username):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mmasterfilepath = mabspath + '/' + filename

        if not os.path.isfile(mmasterfilepath):
            return MESSAGE_13
        elif os.path.islink(mmasterfilepath):
            return EXTRA_MESSAGE_4

        try:
            user = User.get((User.username == username))
        except User.DoesNotExist:
            return MESSAGE_16

        hasRecord = False
        for info in ShareInfo.select().where(ShareInfo.master_user == self.user.username,
                                             ShareInfo.slave_user == username,
                                             ShareInfo.master_file == mmasterfilepath):
            os.remove(info.slave_file)
            info.delete_instance()
            hasRecord = True

        if not hasRecord:
            return MESSAGE_18

        return MESSAGE_17

    def share_dir(self, path, filename, username):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mmasterfilepath = mabspath + '/' + filename

        if not os.path.isdir(mmasterfilepath):
            return MESSAGE_13
        elif os.path.islink(mmasterfilepath):
            return EXTRA_MESSAGE_4

        try:
            user = User.get((User.username == username))
        except User.DoesNotExist:
            return MESSAGE_16

        mslavefilepath = os.path.abspath('sandbox/' + username + '/' + filename)
        if os.path.exists(mslavefilepath):
            return MESSAGE_11

        try:
            shareinfo = ShareInfo.get(
                (ShareInfo.master_file == mmasterfilepath) & (ShareInfo.slave_user == username))
            if shareinfo is not None:
                return EXTRA_MESSAGE_5
        except ShareInfo.DoesNotExist:
            pass

        if username == self.user.username:
            return EXTRA_MESSAGE_6

        newinfo = ShareInfo(master_user=self.user.username, slave_user=username, master_file=mmasterfilepath,
                            slave_file=mslavefilepath)
        newinfo.save()
        os.symlink(mmasterfilepath, mslavefilepath, True)
        return MESSAGE_15

    def unshare_dir(self, path, filename, username):
        try:
            mabspath = self.get_abs_path(path)

            if not os.path.isdir(mabspath):
                return MESSAGE_9
        except:
            return MESSAGE_9

        mmasterfilepath = mabspath + '/' + filename

        if not os.path.isfile(mmasterfilepath):
            return MESSAGE_13
        elif os.path.islink(mmasterfilepath):
            return EXTRA_MESSAGE_4

        try:
            user = User.get((User.username == username))
        except User.DoesNotExist:
            return MESSAGE_16

        hasRecord = False
        for info in ShareInfo.select().where(ShareInfo.master_user == self.user.username,
                                             ShareInfo.slave_user == username,
                                             ShareInfo.master_file == mmasterfilepath):
            os.remove(info.slave_file)
            info.delete_instance()
            hasRecord = True

        if not hasRecord:
            return MESSAGE_18

        return MESSAGE_17

    def slave_change(self, slaveoldpath, slavenewpath):
        for info in ShareInfo.select().where(ShareInfo.slave_file == slaveoldpath,
                                             ShareInfo.slave_user == self.user.username):
            info.slave_file = slavenewpath
            info.save()

    def slave_copy(self, slaveoldpath, slavenewpath):
        try:
            info = ShareInfo.get((ShareInfo.slave_file == slaveoldpath) & (ShareInfo.slave_user == self.user.username))
            newinfo = ShareInfo()
            newinfo.slave_user = self.user.username
            newinfo.slave_file = slavenewpath
            newinfo.master_user = info.master_user
            newinfo.master_file = info.master_file
            newinfo.save()
            pass
        except ShareInfo.DoesNotExist:
            pass

    def slave_remove(self, slavepath):
        for info in ShareInfo.select().where(ShareInfo.slave_file == slavepath,
                                             ShareInfo.slave_user == self.user.username):
            info.delete_instance()

    def master_change(self, masteroldpath, masternewpath):
        isdirectory = os.path.isdir(masteroldpath)
        for info in ShareInfo.select().where(ShareInfo.master_file == masteroldpath,
                                             ShareInfo.master_user == self.user.username):
            os.unlink(info.slave_file)
            os.symlink(masternewpath, info.slave_file, isdirectory)
            info.master_file = masternewpath
            info.save()

    def master_remove(self, masteroldpath):
        for info in ShareInfo.select().where(ShareInfo.master_file == masteroldpath,
                                             ShareInfo.master_user == self.user.username):
            os.remove(info.slave_file)
            info.delete_instance()

    @staticmethod
    def remove_account_cleanup(username):
        for info in ShareInfo.select().where(ShareInfo.master_user == username):
            os.remove(info.slave_file)
            info.delete_instance()

        for info in ShareInfo.select().where(ShareInfo.slave_user == username):
            info.delete_instance()
